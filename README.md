# Convivial Core

Shared functionality for the Convivial profile. Unless another module
requires this module, you shouldn't have a reason to use it.

## INTRODUCTION

Shared functionality for Convivial CXP.

## REQUIREMENTS

None.

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](https://drupal.org/node/1897420).

## CONFIGURATION

Go to /admin/config/convivial and set all the configuration options.

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
