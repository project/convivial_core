<?php

namespace Drupal\convivial_core_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Convivial Core test form.
 */
class ConvivialCoreTestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'convivial_core_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test checkbox'),
      '#description' => $this->t('Test checkbox.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty Submit form method.
  }

}
