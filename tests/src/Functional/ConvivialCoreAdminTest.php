<?php

namespace Drupal\Tests\convivial_core\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Convivial Core administration.
 *
 * @group custom_meta
 */
class ConvivialCoreAdminTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'convivial_core',
    'convivial_core_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this
      ->drupalCreateUser([
        'access convivial administration pages',
      ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the custom meta administration end to end.
   */
  public function testConvivialCoreAdministration() {
    $this->drupalGet('/admin/config/convivial');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Convivial CXP');
  }

}
